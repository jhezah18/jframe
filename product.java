import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.awt.HeadlessException;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupA
 */
public class product extends javax.swing.JFrame {

    /**
     * Creates new form CustomerField
     */
    public product() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jColorChooser1 = new javax.swing.JColorChooser();
        jColorChooser3 = new javax.swing.JColorChooser();
        jColorChooser2 = new javax.swing.JColorChooser();
        title = new javax.swing.JLabel();
        Pname = new javax.swing.JLabel();
        Pprice = new javax.swing.JLabel();
        quantity = new javax.swing.JLabel();
        quaninput = new javax.swing.JTextField();
        priceinput = new javax.swing.JTextField();
        nameinput = new javax.swing.JTextField();
        BtnAdd = new javax.swing.JButton();
        BtnUpdate = new javax.swing.JButton();
        BtnDelete = new javax.swing.JButton();
        BtnRetrieve = new javax.swing.JButton();
        ID = new javax.swing.JLabel();
        inputid = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        category = new javax.swing.JLabel();
        catinput = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        setForeground(new java.awt.Color(0, 0, 0));
        setIconImages(null);

        title.setBackground(new java.awt.Color(255, 204, 204));
        title.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        title.setText("Product Management System");

        Pname.setBackground(new java.awt.Color(255, 204, 204));
        Pname.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        Pname.setText("Product Name");

        Pprice.setBackground(new java.awt.Color(255, 204, 204));
        Pprice.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        Pprice.setText("Product Price");

        quantity.setBackground(new java.awt.Color(255, 204, 204));
        quantity.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        quantity.setText("Quantity");

        quaninput.setBackground(new java.awt.Color(255, 204, 204));
        quaninput.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        quaninput.setBorder(null);
        quaninput.setPreferredSize(new java.awt.Dimension(40, 25));
        quaninput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quaninputActionPerformed(evt);
            }
        });

        priceinput.setBackground(new java.awt.Color(255, 204, 204));
        priceinput.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        priceinput.setBorder(null);
        priceinput.setPreferredSize(new java.awt.Dimension(40, 25));

        nameinput.setBackground(new java.awt.Color(255, 204, 204));
        nameinput.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        nameinput.setBorder(null);
        nameinput.setPreferredSize(new java.awt.Dimension(40, 25));
        nameinput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameinputActionPerformed(evt);
            }
        });

        BtnAdd.setBackground(new java.awt.Color(0, 153, 204));
        BtnAdd.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        BtnAdd.setText("ADD");
        BtnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnAddMouseClicked(evt);
            }
        });
        BtnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAddActionPerformed(evt);
            }
        });

        BtnUpdate.setBackground(new java.awt.Color(0, 204, 0));
        BtnUpdate.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        BtnUpdate.setText("UPDATE");
        BtnUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnUpdateMouseClicked(evt);
            }
        });

        BtnDelete.setBackground(new java.awt.Color(204, 0, 0));
        BtnDelete.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        BtnDelete.setText("DELETE");
        BtnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnDeleteMouseClicked(evt);
            }
        });
        BtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDeleteActionPerformed(evt);
            }
        });

        BtnRetrieve.setBackground(new java.awt.Color(102, 255, 102));
        BtnRetrieve.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        BtnRetrieve.setText("RETRIEVE");
        BtnRetrieve.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnRetrieveMouseClicked(evt);
            }
        });

        ID.setBackground(new java.awt.Color(255, 204, 204));
        ID.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        ID.setText("ID ");

        inputid.setBackground(new java.awt.Color(255, 204, 204));
        inputid.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        inputid.setBorder(null);
        inputid.setPreferredSize(new java.awt.Dimension(41, 25));
        inputid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputidActionPerformed(evt);
            }
        });

        category.setBackground(new java.awt.Color(255, 204, 204));
        category.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        category.setText("Category");

        catinput.setBackground(new java.awt.Color(255, 204, 204));
        catinput.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        catinput.setBorder(null);
        catinput.setPreferredSize(new java.awt.Dimension(40, 25));
        catinput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catinputActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(167, 167, 167)
                .addComponent(title)
                .addGap(105, 105, 105)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(157, 157, 157)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(catinput, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(Pprice, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(Pname, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(ID, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(category, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BtnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BtnUpdate)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(quaninput, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(priceinput, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nameinput, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(inputid, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(BtnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(BtnRetrieve)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabel1)
                        .addGap(56, 56, 56))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ID, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputid, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nameinput, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(Pname, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(priceinput, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(Pprice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(quaninput, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(catinput, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(category, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnRetrieve)
                    .addComponent(BtnDelete)
                    .addComponent(BtnUpdate)
                    .addComponent(BtnAdd))
                .addContainerGap(116, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nameinputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameinputActionPerformed
         
    }//GEN-LAST:event_nameinputActionPerformed

    private void quaninputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quaninputActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quaninputActionPerformed

    private void BtnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDeleteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnDeleteActionPerformed

    private void BtnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAddActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnAddActionPerformed

    private void BtnAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnAddMouseClicked
       
        
        
        try {
           Class.forName("com.mysql.jdbc.Driver"); //load the driver
            try (Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/product_inventory", "root", "") //establishes the connection
            ) {
                Statement stmt = (Statement) con.createStatement(); //get the connection stream(connection port)
                String query = "INSERT products (`id`,`name`, `price`, `quantity`,`category`) VALUES ('"+this.inputid.getText()+"','"+this.nameinput.getText()+"','"+this.priceinput.getText()+"', '"+this.quaninput.getText()+"', '"+this.catinput.getText()+"')";
                stmt.executeUpdate(query);
                JOptionPane.showMessageDialog(null, "Successfully ADDED!");
            } //get the connection stream(connection port)
       } catch (HeadlessException | ClassNotFoundException | SQLException e) {
           System.out.println(e);
       }

        
    }//GEN-LAST:event_BtnAddMouseClicked

    private void BtnUpdateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnUpdateMouseClicked
     Update update = new Update();
         update.setVisible(true);
         this.dispose(); 
       
    }//GEN-LAST:event_BtnUpdateMouseClicked

    private void BtnDeleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnDeleteMouseClicked
        Delete one= new Delete();
        one.setVisible(true);
         this.dispose();
    }//GEN-LAST:event_BtnDeleteMouseClicked

    private void BtnRetrieveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnRetrieveMouseClicked
         Retrieve one= new Retrieve();
        one.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnRetrieveMouseClicked

    private void inputidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputidActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inputidActionPerformed

    private void catinputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_catinputActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_catinputActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(product.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(product.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(product.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(product.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new product().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAdd;
    private javax.swing.JButton BtnDelete;
    private javax.swing.JButton BtnRetrieve;
    private javax.swing.JButton BtnUpdate;
    private javax.swing.JLabel ID;
    private javax.swing.JLabel Pname;
    private javax.swing.JLabel Pprice;
    private javax.swing.JLabel category;
    private javax.swing.JTextField catinput;
    private javax.swing.JTextField inputid;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JColorChooser jColorChooser2;
    private javax.swing.JColorChooser jColorChooser3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField nameinput;
    private javax.swing.JTextField priceinput;
    private javax.swing.JTextField quaninput;
    private javax.swing.JLabel quantity;
    private javax.swing.JLabel title;
    // End of variables declaration//GEN-END:variables
}
